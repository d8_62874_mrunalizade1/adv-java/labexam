<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style>
table, th, td, tr {
	border: 2px solid black;
	border-collapse: collapse;
	border-style: double;
}

h5 {
	text-align: center;
}

</style>
<title>Insert title here</title>
</head>
<body>
	<table style="background-color: lightgrey; margin: auto">
		<h5>List of Countries</h5>
		<tr style="border: 1px solid black;">
			<th>product Id</th>
			<th>product Name</th>
			<th>product Description</th>
			<th>product Quentity</th>
			<th>product price</th>
			<th>delete Product</th>
		</tr>
		<c:forEach var="cntry" items="${requestScope.product_ist}">
			<tr>
				<td>${product.id}</td>
				<td>${product.name}</td>
				<td>${product.desc}</td>
				<td>${product.qnty}</td>
				<td>${product.price}</td>
				<td><a
					href="<spring:url value='/product/delete?Id=${poduct.id}'/>">Delete
						product</a></td>
			</tr>
		</c:forEach>
	</table>
	<h5>
		<a href="<spring:url value='/country/add'/>">Add New product</a>
	</h5>

</body>
</html>