<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style>
table, th, td, tr {
	border: 2px solid black;
	border-collapse: collapse;
	border-style: double;
}

h5 {
	text-align: center;
}
///Product_Tb (p_Id (pk),p_Name,p_Desc,p_Qnty,p_Price,u_Id(fk));
</style>
<title>Insert title here</title>
</head>
<body>
	<form:form method="post" modelAttribute="product">
		<table style="background-color: lightgrey; margin: auto">
			<tr>
				<td>product Name</td>
				<td><form:input path="name" /></td>
				<td><form:errors path="name" /></td>
			</tr>
			<tr>
				<td>Product Price</td>
				<td><form:input path="desc" /></td>
				<td><form:errors path="desc" /></td>
			</tr>
			<tr>
				<td>Country Population</td>
				<td><form:input  path="qnty" /></td>
				<td><form:errors path="qnty" /></td>
			</tr>
				<tr>
				<td>Country Population</td>
				<td><form:input  path="price" /></td>
				<td><form:errors path="price" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="AddProduct" /></td>
			</tr>
		</table>
	</form:form>
</body>
</html>
