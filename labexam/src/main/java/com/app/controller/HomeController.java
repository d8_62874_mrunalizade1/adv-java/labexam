package com.app.controller;



import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.app.entities.Role;
import com.app.entities.User;
import com.app.service.IUserService;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping
@Slf4j
public class HomeController {
	@Autowired
	private IUserService userService;
@GetMapping("/")
public String index()
{
	return("/home/index");
}
@GetMapping("/login")
public String showLoginForm()
{
	return "/user/login";
}
@PostMapping("/login")
public String processLoginForm(@RequestParam String name,@RequestParam String password,Model map,HttpSession session)
{
	try {
        User user=	userService.findByNameAndPassword(name, password);
        session.setAttribute("user", user);
	     if(user.getRole()==Role.ADMIN)
	    	 return "redirect:/admin/home";
	     return "redirect:/teacher/home";
	}
	catch (RuntimeException e) {
		log.error("error occuered",e);
		map.addAttribute("mesg","Invalid name or Password");
		return "/user/login";
		
	}
	
}
}