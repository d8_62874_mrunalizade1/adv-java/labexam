package com.app.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.service.IProductService;

@Controller
@RequestMapping("/admin")
public class ProductController {
@Autowired
private IProductService prodService;
@GetMapping("/home")
public String showAllProducts(HttpSession session,Model map) {
	map.addAttribute("products",prodService.getAllProducts());
	return "/admin/home";
}
}