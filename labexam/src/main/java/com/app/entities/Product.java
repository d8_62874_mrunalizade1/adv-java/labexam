package com.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "product")
@Getter
@Setter
@ToString(exclude = "u_id")

///Product_Tb (p_Id (pk),p_Name,p_Desc,p_Qnty,p_Price,u_Id(fk));
public class Product extends BaseEntity {
	@Column(length = 20)
	@NotBlank
	private String name;
	@Column(length = 50)
	@NotBlank
	private String desc;
	@NotBlank
	private int qnty;
	@NotBlank
	private double price;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "u_id",nullable = false)
	private User user;
	
}
