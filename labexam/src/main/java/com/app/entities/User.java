package com.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

//User_Tb (u_Id (pk),userName,password,role(Admin/User));
@Entity
@Table(name = "user")
@Getter
@Setter
@ToString(exclude = "password")
public class User extends BaseEntity {
	
@Column(unique = true,length = 20)	
private String userName;
@Column(nullable = false)
@NotBlank
private String password;
@Enumerated(EnumType.STRING)
private Role role;
}
